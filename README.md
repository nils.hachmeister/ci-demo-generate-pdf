---
papersize: a4
lang: de-DE
links-as-notes: true
---

# Pamphlet

## Ziele

Wir wollen:

-   Weltfrieden
-   Klimagerechtigkeit
-   Bedingungsloses Grundeinkommen


## Lizenz

[CC0 1.0 Universell](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
